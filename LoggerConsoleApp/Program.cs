﻿using System;
using LoggerTask;
using Microsoft.Extensions.Logging;

namespace LoggerConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            IAlgorithm algorithm = new TestAlgorithm();

            AlgorithmExtension.Factory = LoggerFactory.Create(builder => builder.AddConsole());

            algorithm.CalculateWithLogging(1, 10000);

            try
            {
                algorithm.CalculateWithLogging(20, -1);
            }
            catch(Exception)
            {
            }
        }
    }
}
