﻿using System;
using System.Diagnostics;
using Microsoft.Extensions.Logging;

namespace LoggerTask
{
    public static class AlgorithmExtension
    {
        public static ILoggerFactory Factory { set; get; }

        public static int CalculateWithTimer(this IAlgorithm algorithm, int first, int second, out long elapsedTicks)
        {
            var stopWatch = Stopwatch.StartNew();

            var algorithmResult = algorithm.Calculate(first, second);

            stopWatch.Stop();
            elapsedTicks = stopWatch.ElapsedTicks;

            return algorithmResult;
        }

        public static int CalculateWithLogging(this IAlgorithm algorithm, int first, int second)
        {
            if (Factory is null)
            {
                throw new ArgumentException($"{nameof(Factory)} should be set first.");
            }

            var logger = Factory.CreateLogger("Algorithm profiler:");

            try
            {
                var stopWatch = Stopwatch.StartNew();

                var algorithmResult = algorithm.Calculate(first, second);

                stopWatch.Stop();
                var elapsedTime = stopWatch.Elapsed;

                string stringRepresentation = String.Format("{0:00}h:{1:00}m:{2:00}s:{3:00}ms",
                    elapsedTime.Hours, elapsedTime.Minutes, elapsedTime.Seconds, elapsedTime.Milliseconds);
                logger.LogInformation($"Execution time - " + stringRepresentation);

                return algorithmResult;
            }
            catch (Exception exception)
            {
                logger.LogError(exception.Message);
                throw exception;
            }
        }
    }
}
