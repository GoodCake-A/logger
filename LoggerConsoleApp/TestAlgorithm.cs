﻿using System;
using System.Collections.Generic;
using System.Text;
using LoggerTask;

namespace LoggerConsoleApp
{
    public class TestAlgorithm:IAlgorithm
    {
        public int Calculate(int first, int second)
        {
            if(second < 0 )
            {
                throw new ArgumentOutOfRangeException($"{nameof(second)} should be greater or equal to zero.");
            }

            int result = 0;

            for(int i=0;i<second;i++)
            {
                for(int j=0;j<second;j++)
                {
                    result += first;
                }
            }

            return result;
        }
    }
}
